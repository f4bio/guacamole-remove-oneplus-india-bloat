#!/bin/sh

CURRENTDIR=$(pwd)
MODID=remove-oneplus-india-bloat

rm -f $MODID.zip*

cd ./module
zip -0 -r -D ../$MODID.zip *
cd $CURRENTDIR

rhash --md5 -o $MODID.zip.md5 $MODID.zip
