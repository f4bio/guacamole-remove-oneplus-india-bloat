# Guacamole: Remove OnePlus/India Bloat

## References / Thanks

* https://forums.oneplus.com/threads/red-cable-club-should-be-removed-from-settings-page-how-to-remove.1215999/#post-21649650
* https://www.reddit.com/r/oneplus/comments/edqfku/psa_get_rid_of_oneplus_cloud_service_from_newest/?utm_source=share&utm_medium=web2x

## How To

1) flash zip in recovery

## What it does

* deletes `/data/india/india.img`
* uninstalls
  * OnePlus Account `com.oneplus.membership`
  * OnePlus Cloud `com.heytap.cloud`, `com.oneplus.cloud.basiccolorwhite.overlay`, `com.oneplus.cloud.basiccolorwhite.overlay`
  * `com.oneplus.opwlb`
  * OnePlus Filemanager `com.oneplus.filemanager`, `com.oneplus.filemanager.black.overlay`, `com.oneplus.filemanager.white.overlay`
  * Netflix `com.netflix.mediaclient`
